#include "UnitTest++.h"

#include "test_utils.hh"

#include "geners/forward_listIO.hh"
#include "geners/StringArchive.hh"
#include "geners/Record.hh"
#include "geners/Reference.hh"

using namespace gs;
using namespace std;

#ifdef CPP11_STD_AVAILABLE

namespace {
    TEST(forward_listIO)
    {
        StringArchive ar0("archive");

        forward_list<string> l0;
        forward_list<string> l1;
        l1.push_front("foo");
        forward_list<int> l2;
        l2.push_front(123);
        l2.push_front(-456);

        ar0 << Record(l0, "l0", "top");
        ar0 << Record(l1, "l1", "top");
        ar0 << Record(l2, "l2", "top");

        // Get them back
        decltype(l1) l1_read;
        Reference<decltype(l1)>(ar0, "l1", "top").restore(0,&l1_read);
        CHECK(l1_read == l1);

        decltype(l0) l0_read;
        Reference<decltype(l0)>(ar0, "l0", "top").restore(0,&l0_read);
        CHECK(l0_read == l0);

        decltype(l2) l2_read;
        Reference<decltype(l2)> ref(ar0, "l2", "top");
        ref.restore(0,&l2_read);
        CHECK(l2_read == l2);

        std::unique_ptr<decltype(l2)> p2 = ref.get(0);
        CHECK(*p2 == l2);

        std::shared_ptr<decltype(l2)> p3 = ref.getShared(0);
        CHECK(*p3 == l2);
    }
}

#else // CPP11_STD_AVAILABLE

namespace {
    TEST(forward_listIO)
    {
    }
}

#endif // CPP11_STD_AVAILABLE
