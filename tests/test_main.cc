#include "UnitTest++.h"
#include "TestReporterStdout.h"
#include "test_utils.hh"

#include <iostream>

int main(int /* argc */, char const* argv[])
{
    std::cout << "Running " << parse_progname(argv[0])
              << " test suite." << std::endl;
    return UnitTest::RunAllTests();
}
