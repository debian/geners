#include "UnitTest++.h"

#include "test_utils.hh"

#include <sstream>

#include "geners/ColumnBuffer.hh"
#include "geners/binaryIO.hh"

using namespace gs;
using namespace std;

namespace {
    TEST(ColumnBuffer)
    {
        Private::ColumnBuffer b1;
        b1.podsize = sizeof(test_rng());
        for (unsigned i=0; i<100; ++i)
        {
            write_pod(b1.buf, test_rng());
            ++b1.lastrowp1;
        }

        std::ostringstream os;
        CHECK(b1.classId().write(os));
        CHECK(b1.buf.classId().write(os));
        CHECK(b1.write(os));

        std::istringstream is(os.str());
        Private::ColumnBuffer b2;
        ClassId bufid(is, 1);
        CHECK(bufid == b1.classId());
        ClassId cid(is, 1);
        CHECK(cid == b1.buf.classId());
        Private::ColumnBuffer::restore(bufid, cid, is, &b2);
        CHECK(b1 == b2);
    }
}
