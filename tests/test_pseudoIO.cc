#include <sstream>
#include "UnitTest++.h"

#include "geners/pseudoIO.hh"
#include "geners/StringArchive.hh"
#include "geners/Record.hh"
#include "geners/Reference.hh"

using namespace gs;

struct Dummy
{
    inline Dummy() : i_(7) {}

    inline bool operator==(const Dummy& r) const {return i_ == r.i_;}
    inline bool operator!=(const Dummy& r) const {return i_ != r.i_;}

private:
    int i_;
};

gs_enable_pseudo_io(Dummy)

namespace {
    TEST(pseudoIO)
    {
        StringArchive ar;

        Dummy d;
        ar << Record(d, "dummy", "haha");

        const Dummy cd;
        ar << Record(cd, "dummy2", "haha");

        const volatile Dummy cvd;
        ar << Record(cvd, "dummy3", "haha");

        volatile Dummy vd;
        ar << Record(vd, "dummy4", "haha");

        Reference<Dummy> ref(ar, "dummy", "haha");
        CHECK(ref.unique());

        Reference<Dummy> ref2(ar, "dummy4", "haha");
        CHECK(ref2.unique());

        Dummy d1;
        ref.restore(0, &d1);
        CHECK(d == d1);

        CHECK(d == *ref2.get(0));
        CHECK(d == *ref.getShared(0));
    }
}
