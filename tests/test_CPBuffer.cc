#include "UnitTest++.h"

#include "test_utils.hh"

#include "geners/StringArchive.hh"
#include "geners/CPBufferRecord.hh"
#include "geners/CPBufferReference.hh"
#include "geners/binaryIO.hh"

using namespace gs;
using namespace std;

namespace {
    TEST(CPBufferIO)
    {
        StringArchive ar0("archive");

        Private::ColumnBuffer b1;
        b1.podsize = sizeof(test_rng());
        for (unsigned i=0; i<100; ++i)
        {
            write_pod(b1.buf, test_rng());
            ++b1.lastrowp1;
        }

        unsigned long col = 10;
        Private::CPBufferRecord rec(b1, "dummy", "test", col);
        ar0 << rec;
        const unsigned long long id = rec.id();

        ClassId id1(ClassId::makeId<Private::ColumnBuffer>());
        ClassId id2(ClassId::makeId<CharBuffer>());
        Private::CPBufferReference ref(ar0, id1, id2, id);
        CHECK(ref.unique());

        Private::ColumnBuffer b2;
        unsigned long col2 = 0;
        ref.restore(0, &b2, &col2);
        CHECK(b1 == b2);
        CHECK_EQUAL(col, col2);
    }
}
