#ifndef GSTEST_SERIALIZABLEDERIVEDC_HH_
#define GSTEST_SERIALIZABLEDERIVEDC_HH_

#include <iostream>

#include "SerializableDerivedB.hh"

class SerializableDerivedC : public SerializableDerivedB
{
public:
    inline SerializableDerivedC(const unsigned u, const double d)
        : SerializableDerivedB(d), u_(u) {}

    inline ~SerializableDerivedC() {}

    inline unsigned u() const {return u_;}

    inline gs::ClassId classId() const {return gs::ClassId(*this);}
    bool write(std::ostream& of) const;

    static inline const char* classname() {return "SerializableDerivedC";}
    static inline unsigned version() {return 1;}
    static SerializableDerivedC* read(const gs::ClassId& id, std::istream& in);

protected:
    virtual bool isEqual(const SerializableBase& otherBase) const
    {
        // Note the call of "isEqual" method of the base class.
        // This should be done if that method is not pure virtual.
        const SerializableDerivedC& r = 
            static_cast<const SerializableDerivedC&>(otherBase);
        return SerializableDerivedB::isEqual(otherBase) && u_ == r.u_;
    }

private:
    unsigned u_;
};

#endif // GSTEST_SERIALIZABLEDERIVEDC_HH_
