#include <cassert>

#include "SerializableDerivedC.hh"

#include "geners/binaryIO.hh"
#include "geners/CPP11_auto_ptr.hh"
#include "geners/IOException.hh"

bool SerializableDerivedC::write(std::ostream& of) const
{
    gs::write_pod(of, b());
    gs::write_pod(of, u_);
    return !of.fail();
}

SerializableDerivedC* SerializableDerivedC::read(const gs::ClassId& id,
                                                 std::istream& in)
{
    // We do not expect any other class to be derived from
    // this one (note that the destructor is not virtual).
    // Because of this, we only need to handle the case
    // of correct class id.
    static const gs::ClassId myId(gs::ClassId::makeId<SerializableDerivedC>());
    myId.ensureSameId(id);

    double b(0.0);
    gs::read_pod(in, &b);
    unsigned u;
    gs::read_pod(in, &u);
    if (in.fail())
        throw gs::IOReadFailure("In SerializableDerivedC::read: "
                                "input stream failure");
    return new SerializableDerivedC(u, b);
}
