#include <cassert>
#include <sstream>

#include "UnitTest++.h"
#include "test_utils.hh"
#include "SerializableDerivedA.hh"
#include "SerializableDerivedB.hh"
#include "SerializableDerivedC.hh"

#include "geners/Record.hh"
#include "geners/Reference.hh"
#include "geners/BinaryFileArchive.hh"

using namespace gs;
using namespace std;

namespace {
    TEST(ArchiveIO_inheritance)
    {
        BinaryFileArchive ar("archive_i", "w+", "archive with inheritance");

        const SerializableDerivedA a(1);
        const SerializableDerivedB b(0.1234);
        const SerializableBase* pb = &b;

        ar << Record(dynamic_cast<const SerializableBase&>(a), "item A", "")
           << Record(*pb, "item B", "");

        Reference<SerializableBase> ra(ar, "item A", "");
        CHECK(ra.unique());
        CPP11_auto_ptr<SerializableBase> p1 = ra.get(0);
        CHECK(*p1 == a);

        Reference<SerializableBase> rb(ar, "item B", "");
        CHECK(rb.unique());
        CPP11_shared_ptr<SerializableBase> p2 = rb.getShared(0);
        CHECK(dynamic_cast<SerializableDerivedB&>(*p2) == b);
    }
}
